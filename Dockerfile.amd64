ARG ALPINE_VERSION=3.16

FROM alpine as qemu

RUN if [ -n "x86_64" ]; then \
		wget -O /qemu-x86_64-static https://github.com/multiarch/qemu-user-static/releases/download/v4.1.0-1/qemu-x86_64-static; \
	else \
		echo '#!/bin/sh\n\ntrue' > /qemu-x86_64-static; \
	fi; \
	chmod a+x /qemu-x86_64-static

FROM amd64/alpine:$ALPINE_VERSION as builder

COPY --from=qemu /qemu-x86_64-static /usr/bin/

COPY busybox /work/busybox

RUN apk add build-base linux-headers perl; \
	cd /work/busybox; \
	make defconfig; \
	make -j4 CONFIG_STATIC=y

FROM amd64/alpine:${ALPINE_VERSION} as staging

COPY --from=qemu /qemu-x86_64-static /usr/bin/

# INSTALL RUNTIME DEPENDENCIES

# COPY ARTIFACTS FROM OTHER CONTAINERS

RUN rm -rf /var/cache/apk

# remove all but the essential busybox links
RUN for i in `/bin/busybox --list | grep -v '^rm$' | grep -v '^sh$'`; do rm -f /sbin/$i /bin/$i; done; rm -f /bin/rm

# add back files and busybox
COPY files/ /

COPY --from=builder /work/busybox/busybox /bin/

# make busybox create its own links
RUN /bin/busybox --install -s /bin/


FROM scratch

COPY --from=staging / /

# CONFIGURATION

CMD [ "/entrypoint" ]

